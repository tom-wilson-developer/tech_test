﻿using System;
namespace FrogPondAPI.CoreAPI
{
    public enum Error
    {
        UnknownError = 1000,
        PondNotFound,
        FrogNotFound,
        PredatorNotFound,
        MalformedRequestBody,
        InvalidFrogCount,
        FrogsSameGender,
        DifferentPonds,
        AlreadyMated,
        FailedToWrite,
        FailedToBreed

    }

}
