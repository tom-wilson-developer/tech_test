﻿using System;
using FrogPondAPI.CoreAPI;

namespace FrogPondAPI.CoreAPI
{
    //This is a basic implementation. I think ideally there should be subclasses which
    //expand the response to include more specific properties than Object Data.
    public class Response
    {

        public Boolean Success { get; set; }
        public Error? Error { get; set; }
        public String ErrorMessage { get; set; }
        public Object Data;

        public Response(Boolean success, Error? error)
        {
            Success = success;
            if(error != null){
                Error = (Error)error;
                ErrorMessage = Utils.GetHumanReadableErrorString((Error)error);
            }

        }

        public Response(Boolean success, Object data, Error? error){
            Success = success;
            if(error != null){
                Error = error;
                ErrorMessage = Utils.GetHumanReadableErrorString((Error)error);
            }
            Data = data;
        }
    }
}
