﻿using System;
namespace FrogPondAPI.CoreAPI
{
    static class Utils
    {

        public static String GetHumanReadableErrorString(Error error)
        {
            switch (error)
            {
                case Error.PondNotFound:
                    return "Pond not found";
                case Error.FrogNotFound:
                    return "Frog not found";
                case Error.MalformedRequestBody:
                    return "Unable to parse request body";
                case Error.InvalidFrogCount:
                    return "Can only breed with two frogs";
                case Error.FrogsSameGender:
                    return "Can only breed frogs of different gender";
                case Error.DifferentPonds:
                    return "Can only breed frogs from the same pond";
                case Error.AlreadyMated:
                    return "Frogs can only mate once a day";
                case Error.FailedToWrite:
                    return "Failed to write to DB";
                case Error.FailedToBreed:
                    return "The frogs failed to breed today";
                default:
                    return "Unknown error occured";
            }
        }
    }

}
