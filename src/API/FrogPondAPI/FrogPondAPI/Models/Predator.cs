﻿using System;
using System.Collections.Generic;
using FrogPondAPI.Contexts;
using Microsoft.Extensions.DependencyInjection;

namespace FrogPondAPI.Models
{
    public class Predator : Animal
    {
        public Animal Target { get; internal set; }

        public Predator()
        {
            //Preditors tend to live longer than their prey. And not just because they eat them.
            this.LifeSpan = 15;
            this.Agility = 5;
        }

	}
}
