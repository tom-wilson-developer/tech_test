﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace FrogPondAPI.Models
{
    public class Animal
    {

        [JsonConverter(typeof(StringEnumConverter))]
        public enum LivingStateEnum
        {
            Alive,
            Dead
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public enum DeathReasonEnum
        {
            OldAge,
            Eaten,
            Starvation
        }
        public long Id { get; protected set; }
        public LivingStateEnum State { get; protected set; }
        DeathReasonEnum? _deathReason;
        public DeathReasonEnum? DeathReason { 
            get { return _deathReason; } 
            set{
                State = LivingStateEnum.Dead;
                _deathReason = value;
            }
        }
        protected int LifeSpan;
        public int Age { get; protected set; }

        //A property to determine how good something is a getting what it wants
        //be it food or running away.
        public int Agility { get; protected set; }
        static public int MaxAgility = 10;

        [Required]
        public long? PondId { get; set; }

        public Animal()
        {
            Age = 0;
            State = LivingStateEnum.Alive;
        }


        public virtual void AgeOne()
        {
            if (State == LivingStateEnum.Alive)
            {
                Age++;
            }

            if (Age >= LifeSpan)
            {
                DeathReason = DeathReasonEnum.OldAge;
            }
        }

    }
}
