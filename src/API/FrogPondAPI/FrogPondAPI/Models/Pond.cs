﻿using System;
using System.Collections.Generic;
namespace FrogPondAPI.Models
{
    public class Pond
    {
        public long Id { get; private set; }
        public List<Frog> Frogs { get; set; }
        public List<Predator> Predators { get; set; }

        //Time will be manually pass by calling a Sleep method on the pond controller.
        public int Day { get; internal set; }

        public Pond()
        {
            Frogs = new List<Frog>();
            Predators = new List<Predator>();
        }

    }
}
