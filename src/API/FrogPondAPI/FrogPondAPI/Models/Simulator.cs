﻿using System;
using System.Collections.Generic;
using static FrogPondAPI.Models.Animal;

namespace FrogPondAPI.Models
{
    public class Simulator
    {
        public Simulator()
        {
        }


        public static void SimulateADay(Pond pond){
            pond.Day++;
            foreach (Predator predator in pond.Predators)
            {
                Simulator.SimulateHunting(predator, pond.Frogs);
                predator.AgeOne();
            }
            foreach (Frog frog in pond.Frogs)
            {
                frog.AgeOne();
            }
        }

        public static void SimulateHunting(Predator predator, List<Frog> frogs){
            if (predator.State == Animal.LivingStateEnum.Dead)
            {
                return;
            }
            Random random = new Random();
            if (predator.Target != null && predator.Target.State == LivingStateEnum.Alive)
            {
                // Hunt target

                //The idea is that the animals agility needs to be higher
                //than the chance roll in order for the outcome to go in
                //its favour.

                //Chance for Predator to attack
                int chance = random.Next(0, Animal.MaxAgility + 1);
                if (predator.Agility > chance)
                {
                    //Can attack
                    chance = random.Next(0, Animal.MaxAgility + 1);
                    //Targets chance to defend
                    if (predator.Target.Agility < chance)
                    {
                        predator.Target.DeathReason = DeathReasonEnum.Eaten;
                    }//else it escapes
                }


            }
            else
            {
                // find a new target from list of frogs
                List<Frog> living = frogs.FindAll(f => f.State == Animal.LivingStateEnum.Alive);
                if (living == null || living.Count == 0)
                {
                    predator.DeathReason = DeathReasonEnum.Starvation;
                    return;
                }
                // pick new prey at random.
                int index = random.Next(0, living.Count);
                predator.Target = living[index];
            }
        }
    }
}
