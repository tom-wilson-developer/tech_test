﻿using System;
using FrogPondAPI.CoreAPI;

namespace FrogPondAPI.Models
{
    public class BreedingPen
    {
        static private Random random = new Random();
        public static Frog MateFrogs(Frog frog1, Frog frog2)
        {
            if(frog1.Gender == frog2.Gender){
                throw new BreedingExceptionSameGender();
            }

            if(frog1.PondId != frog2.PondId){
                throw new BreedingExceptionPondOutBounds();
            }

            if(frog1.Mated || frog2.Mated)
            {
                throw new BreedingExceptionAlreadyMated();
            }

            frog1.Mated = true;
            frog2.Mated = true;

            //Chance of success
            if(random.Next(0, Frog.MaxVitality +1) < frog1.Vitality){
                Frog child = new Frog();
                child.PondId = frog2.PondId;
                if (random.Next(0, 101) % 2 == 0)
                {
                    child.Gender = Frog.GenderEnum.Female;
                }

                // New borns need to wait a day.
                child.Mated = true;

                return child;
            }else{
                throw new BreedingExceptionFailed();
            }
        }
    }

    public class BreedingExceptionSameGender : PondException {}
    public class BreedingExceptionPondOutBounds : PondException {}
    public class BreedingExceptionAlreadyMated : PondException {}
    public class BreedingExceptionFailed : PondException {}


}
