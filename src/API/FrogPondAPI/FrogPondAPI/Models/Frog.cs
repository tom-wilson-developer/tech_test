﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace FrogPondAPI.Models
{
    public class Frog : Animal
    {

        [JsonConverter(typeof(StringEnumConverter))]
        public enum GenderEnum
        {
            Male,
            Female
        }

        public GenderEnum Gender { get; set; }
        public Boolean Mated;
        public int Vitality { get; private set; }
        public static int MaxVitality = 10;
        public Frog()
        {
            this.LifeSpan = 10;
            this.Agility = 3;
            this.Vitality = 7;
        }

		public override void AgeOne()
		{
            base.AgeOne();
            Mated = false;
		}
	}
}
