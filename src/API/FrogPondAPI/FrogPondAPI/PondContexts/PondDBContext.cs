﻿using System;
using Microsoft.EntityFrameworkCore;
using FrogPondAPI.Models;

namespace FrogPondAPI.Contexts
{
    //This will be our main database context a new name may need to be thought of to avoid confusion.
    public class PondDBContext : DbContext
    {

        public DbSet<Pond> Ponds { get; set; }
        public DbSet<Frog> Frogs { get; set; }
        public DbSet<Predator> Predators { get; set; }

        public PondDBContext(DbContextOptions<PondDBContext> options)
            : base(options)
        {
        }

        //Playing to see if I could get foreign key constraints working
        /*protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Frog>()
                        .HasOne(f => f.Pond)
                        .WithMany(p => p.Frogs)
                        .HasForeignKey(f => f.PondId)
                .HasConstraintName("ForeignKey_Frog_Pond");
        }*/
    }
}
