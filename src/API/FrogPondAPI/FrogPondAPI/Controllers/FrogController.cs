﻿using System;
using System.Collections.Generic;
using System.Linq;
using FrogPondAPI.Contexts;
using FrogPondAPI.CoreAPI;
using FrogPondAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FrogPondAPI.Controllers
{
    [Route("api/[controller]")]
    public class FrogController : Controller
    {
        private readonly PondDBContext Context;
        public FrogController(PondDBContext context)
        {
            this.Context = context;
        }


        [HttpGet]
        public IEnumerable<Frog> GetAll()
        {
            return Context.Frogs.ToList();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(long id)
        {
            Frog frog = Context.Frogs.FirstOrDefault(f => f.Id == id);

            if(frog == null){
                return new ObjectResult(new Response(false, Error.FrogNotFound));
            }

            return new ObjectResult(new Response(true, frog, null));
        }

        [HttpPost]
        public IActionResult CreateFrog([FromBody] Frog frog)
        {
            if (frog == null)
            {
                return new ObjectResult(new Response(false, Error.MalformedRequestBody));
            }
            // I couldn't figure out how to get foreign key constraits working.
            // It may be different if wasn't working in memory but currently this is
            // the most conveniant place to validate this.
            Pond pond = Context.Ponds.AsNoTracking().FirstOrDefault(p => p.Id == frog.PondId);
            if(pond == null){
                return new ObjectResult(new Response(false, Error.PondNotFound));
            }

            if (ModelState.IsValid)
            {
                try
                {
                    Context.Frogs.Add(frog);
                    Context.SaveChanges();

                    return new ObjectResult(new Response(true, frog, null));
                }
                catch (DbUpdateException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.Write(ex.StackTrace);
                    return new ObjectResult(new Response(false, Error.FailedToWrite));
                }

            }
            else
            {
                return new ObjectResult(new Response(false, Error.MalformedRequestBody));
            }
        }

        [HttpGet("Breed")]
        public IActionResult BreedFrogs(long? frog1Id, long? frog2Id)
        {
            if(frog1Id == null || frog2Id == null){
                return new ObjectResult(new Response(false, Error.InvalidFrogCount));
            }

            Frog frog1 = Context.Frogs.FirstOrDefault(f => f.Id == frog1Id);
            Frog frog2 = Context.Frogs.FirstOrDefault(f => f.Id == frog2Id);

            if(frog1 == null || frog2 == null){
                return new ObjectResult(new Response(false, Error.FrogNotFound));
            }

            try{
                Frog frog = BreedingPen.MateFrogs(frog1, frog2);
                Context.Frogs.Add(frog);

                if (ModelState.IsValid)
                {
                    Context.SaveChanges();
                }else{
                    return new ObjectResult(new Response(false, Error.UnknownError));
                }

                return new ObjectResult(new Response(true, frog, null));
            }catch (BreedingExceptionFailed){
                return new ObjectResult(new Response(false, Error.FailedToBreed));
            }catch(BreedingExceptionSameGender){
                return new ObjectResult(new Response(false, Error.FrogsSameGender));
            }catch(BreedingExceptionPondOutBounds){
                return new ObjectResult(new Response(false, Error.DifferentPonds));
            }catch(BreedingExceptionAlreadyMated){
                return new ObjectResult(new Response(false, Error.AlreadyMated));
            }catch (DbUpdateException ex){
                Console.WriteLine(ex.Message);
                Console.Write(ex.StackTrace);
                return new ObjectResult(new Response(false, Error.FailedToWrite));
            }
        }

    }


}
