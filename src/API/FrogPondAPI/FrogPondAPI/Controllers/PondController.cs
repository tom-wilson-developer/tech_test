﻿using System;
using System.Collections.Generic;
using System.Linq;
using FrogPondAPI.Contexts;
using FrogPondAPI.CoreAPI;
using FrogPondAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FrogPondAPI.Controllers
{
    [Route("api/[controller]")]
    public class PondController : Controller
    {
        private readonly PondDBContext Context;

        public PondController(PondDBContext context)
        {
            this.Context = context;
        }

        [HttpGet]
        public IActionResult AllPonds()
        {
            return new ObjectResult(new Response(true, Context.Ponds.Include(p => p.Frogs)
                                                 .Include(p => p.Predators)
                                                 .ToList(), null));
        }

        [HttpGet("{id}", Name = "PondById")]
        public IActionResult GetById(long id)
        {
            Pond pond = Context.Ponds.Include(p => p.Frogs).Include(p => p.Predators)
                               .ThenInclude(p => p.Target)
                               .FirstOrDefault<Pond>(p => p.Id == id);

            if (pond == null)
            {
                return new ObjectResult(new Response(false, Error.PondNotFound));
            }

            return new ObjectResult(new Response(true, pond, null));
        }

        [HttpPost]
        public IActionResult Create([FromBody] Pond pond)
        {
            if (pond == null)
            {
                return new ObjectResult(new Response(false, Error.MalformedRequestBody));
            }

            Context.Ponds.Add(pond);
            try{
                Context.SaveChanges();
                return new ObjectResult(new Response(true, pond, null));
            }catch (DbUpdateException ex){
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return new ObjectResult(new Response(false, Error.FailedToWrite));
            }

        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id){
            Pond pond = Context.Ponds.Include(p => p.Frogs)
                               .FirstOrDefault<Pond>(p => p.Id == id);

            if (pond == null)
            {
                return new ObjectResult(new Response(false, Error.PondNotFound));
            }

            Context.Remove(pond);
            try
            {
                Context.SaveChanges();
                return new ObjectResult(new Response(true, pond, null));
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return new ObjectResult(new Response(false, Error.FailedToWrite));
            }
        }
 
    }
}
