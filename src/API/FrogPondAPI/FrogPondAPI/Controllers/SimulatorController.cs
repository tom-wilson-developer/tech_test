﻿using System;
using System.Linq;
using FrogPondAPI.Contexts;
using FrogPondAPI.CoreAPI;
using FrogPondAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FrogPondAPI.Controllers
{
    [Route("api/simulate")]
    public class SimulatorController : Controller
    {
        private readonly PondDBContext Context;
        public SimulatorController(PondDBContext context)
        {
            Context = context;
        }

        [HttpGet("{id}")]
        public IActionResult Simulate(long id)
        {
            Pond pond = Context.Ponds.FirstOrDefault<Pond>(p => p.Id == id);
            Context.Entry(pond).Collection(p => p.Frogs).Query().Where(f => f.State == Animal.LivingStateEnum.Alive).Load();
            Context.Entry(pond).Collection(p => p.Predators).Query().Where(p => p.State == Animal.LivingStateEnum.Alive).Load();

            if (pond == null)
            {
                return new ObjectResult(new Response(false, Error.PondNotFound));
            }
            Simulator.SimulateADay(pond);
            try{
                Context.SaveChanges();
                //easest way to get an overview of a pond.
                return new PondController(Context).GetById(pond.Id);
            }catch(DbUpdateException ex){
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return new ObjectResult(new Response(false, Error.FailedToWrite));
            }



        }
    }
}
