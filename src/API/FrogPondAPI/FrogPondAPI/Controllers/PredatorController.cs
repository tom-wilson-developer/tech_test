﻿using System;
using System.Collections.Generic;
using System.Linq;
using FrogPondAPI.Contexts;
using FrogPondAPI.CoreAPI;
using FrogPondAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FrogPondAPI.Controllers
{
    [Route("api/[controller]")]
    public class PredatorController : Controller
    {
        private readonly PondDBContext Context;
        public PredatorController(PondDBContext context)
        {
            Context = context;
        }

        [HttpGet]
        public IEnumerable<Predator> GetAll()
        {
            return Context.Predators.Include(p => p.Target).ToList();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(long id)
        {
            Predator predator = Context.Predators.Include(p => p.Target).FirstOrDefault(f => f.Id == id);

            if (predator == null)
            {
                return new ObjectResult(new Response(false, Error.PredatorNotFound));
            }

            return new ObjectResult(new Response(true, predator, null));
        }

        [HttpPost]
        public IActionResult CreatePredator([FromBody] Predator predator){
            if(predator == null){
                return new ObjectResult(new Response(false, Error.MalformedRequestBody));
            }

            Pond pond = Context.Ponds.FirstOrDefault(p => p.Id == predator.PondId);
            if(pond == null){
                return new ObjectResult(new Response(false, Error.PondNotFound));
            }

            if(ModelState.IsValid){
                try{
                    Context.Predators.Add(predator);
                    Context.SaveChanges();
                    return new ObjectResult(new Response(true, predator, null));
                }catch(DbUpdateException ex){
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    return new ObjectResult(new Response(false, Error.FailedToWrite));
                }
            }else{
                return new ObjectResult(new Response(false, Error.MalformedRequestBody));
            }



        }
    }
}
