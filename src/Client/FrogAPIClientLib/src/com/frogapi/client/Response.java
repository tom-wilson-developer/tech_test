package com.frogapi.client;

import net.maritimecloud.internal.core.javax.json.JsonObject;
import net.maritimecloud.internal.core.javax.json.JsonValue;


class Response {
    //constants
    private static final String SUCCESS_KEY = "success";
    private static final String ERROR_KEY = "error";
    private static final String ERROR_MESSAGE_KEY = "errorMessage";
    private static final String DATA_KEY = "data";


    public Boolean success;
    public Error error;
    public String errorMessage;
    public JsonValue data;

    Response(JsonObject responseJson){
        if(responseJson == null){
            return;
        }
        success = responseJson.getBoolean(SUCCESS_KEY);
        if(!success){
            error = Error.errorFromInt(responseJson.getInt(ERROR_KEY));
            errorMessage = responseJson.getString(ERROR_MESSAGE_KEY);
        }
        if(!responseJson.isNull(DATA_KEY)){
            data = responseJson.get(DATA_KEY);
        }
    }

    Response(Boolean success, Error error){
        this.success = success;
        if(error != null){
            this.error = error;
            errorMessage = error.getClientErrorMessage();
        }
    }
}
