package com.frogapi.client;


import net.maritimecloud.internal.core.javax.json.JsonArray;
import net.maritimecloud.internal.core.javax.json.JsonObject;

import java.util.ArrayList;

public class Pond {

    private static final String ID_KEY = "id";
    private static final String FROGS_KEY = "frogs";
    private static final String PREDATORS_KEY = "predators";
    private static final String DAY_KEY = "day";

    private long id;
    private ArrayList<Frog> frogs = new ArrayList<>();
    private ArrayList<Predator> predators = new ArrayList<>();
    private int day;

    Pond(JsonObject jsonObject){
        if(jsonObject == null){
            throw new NullPointerException("JSON can not be null");
        }

        id = jsonObject.getInt(ID_KEY, 0);
        day = jsonObject.getInt(DAY_KEY, 0);

        JsonArray frogsJson = jsonObject.getJsonArray(FROGS_KEY);
        if (frogsJson != null && frogsJson.size() > 0) {
            for (int i = 0; i < frogsJson.size(); i++) {
                Frog frog = new Frog(frogsJson.getJsonObject(i));
                frogs.add(frog);
            }
        }

        JsonArray predatorJson = jsonObject.getJsonArray(PREDATORS_KEY);
        if(predatorJson != null && predatorJson.size() > 0){
            for(int i=0;i<predatorJson.size(); i++){
                Predator predator = new Predator(predatorJson.getJsonObject(i));
                predators.add(predator);
            }
        }
    }

    public long getId(){
        return id;
    }

    public ArrayList<Frog> getFrogs() {
        return frogs;
    }

    public int getDay() {
        return day;
    }

    public ArrayList<Predator> getPredators() {
        return predators;
    }

    public Frog getFrogWithId(long frogId){
        Frog frog = null;
        for(Frog f : frogs){
            if(f.getId() == frogId){
                frog = f;
                break;
            }
        }
        return frog;
    }

    public Predator getPredatorWithId(long predatorId){
        Predator predator = null;
        for(Predator p : predators){
            if(p.getId() == predatorId){
                predator = p;
                break;
            }
        }
        return predator;
    }
}
