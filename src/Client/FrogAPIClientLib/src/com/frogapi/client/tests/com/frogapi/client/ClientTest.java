package com.frogapi.client;

import org.junit.*;

import java.net.URL;
import java.util.List;
import com.frogapi.client.Client;
import static org.junit.Assert.*;

public class ClientTest {

    Client client;

    @BeforeClass
    public static void setUpAll(){

    }

    @Before
    public void setUp() throws Exception {
        client = new Client(new URL("http://localhost:5000"));
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void createPondTest() {
        Pond pond = createPond();
    }

    private Pond createPond(){
        try {
            Pond pond = client.createPond();
            assertTrue(pond != null);
            assertTrue(pond.getId() > 0);
            return pond;
        }catch(Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }

        return null;
    }

    @Test
    public void deletePondTest(){
        Pond pond = createPond();
        deletePond(pond);
    }

    @Test
    public void getPondsTest() {
        try{
            Pond pond1 = client.createPond();
            Pond pond2 = client.createPond();

            List<Pond> ponds = client.getPonds();
            //Currently no way to clean up between tests so just check if it's greater than the number created.
            assertTrue(ponds.size() >=2);

            Boolean pone1Found = false;
            Boolean pond2Found = false;
            for (Pond p : ponds){
                if(p.getId() == pond1.getId()){
                    pone1Found = true;
                }

                if(p.getId() == pond2.getId()){
                    pond2Found = true;
                }
            }

            assertTrue(pone1Found);
            assertTrue(pond2Found);

        }catch(FrogApiClientException e){
            e.printStackTrace();
           fail(e.getMessage());
        }


    }

    @Test
    public void getPondTest() {
        Pond pond = createPond();
        Pond pond1 = getPond(pond.getId());


        assertTrue(pond.getId() == pond1.getId());
        assertTrue(pond.getFrogs().size() == pond1.getFrogs().size());
        assertTrue(pond.getPredators().size() == pond1.getFrogs().size());
    }

    private Pond getPond(long pondId){
        try{
            return client.getPond(pondId);
        }catch(Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }
        return null;
    }

    private void deletePond(Pond pond){
        try{
            Pond deleted = client.deletePond(pond.getId());

            assertTrue(deleted.getId() == pond.getId());

            try {
                client.deletePond(pond.getId());
            }catch (FrogApiClientException e) {
                assertTrue(e.getError() == Error.PondNotFound);
            }

        }catch (Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void createFrogTest() {
        Pond pond = createPond();
        createFrog(Frog.GenderEnum.Male, pond);
        createFrog(Frog.GenderEnum.Female, pond);
    }

    private Frog createFrog(Frog.GenderEnum genderEnum, Pond pond){

        try{
            Frog frog = client.createFrog(genderEnum, pond.getId());

            assertTrue(frog.pondId == pond.getId());
            assertTrue(frog.id > 0);
            assertTrue(frog.getGender() == genderEnum);
            assertNotNull(frog.getLivingState());
            assertFalse(frog.hasMated());
            assertTrue(frog.getDeathReason() == Animal.DeathReasonEnum.Unknown);

            return frog;
        }catch (Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }

        return null;
    }

    @Test
    public void getFrogTest(){
        Pond pond = createPond();
        Frog frog = createFrog(Frog.GenderEnum.Female, pond);
        try{
            Frog frog2 = client.getFrog(frog.getId());

            assertTrue(frog.getId() == frog2.getId());
            assertTrue(frog.getGender() == frog2.getGender());
            assertTrue(frog.getPondId() == frog2.getPondId());

        }catch(Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }


    }

    @Test
    public void breedFrogsTest() {
        Pond pond = createPond();
        Frog frog1 = createFrog(Frog.GenderEnum.Male, pond);
        Frog frog2 = createFrog(Frog.GenderEnum.Female, pond);

        try{
            Frog child = client.breedFrogs(frog1.getId(),frog2.getId());

            assertTrue(child.getId() != frog1.getId());
            assertTrue(child.getId() != frog2.getId());
            assertTrue(child.getPondId() == frog1.getPondId());

        }catch(FrogApiClientException e){
            if(e.getError() == Error.FailedToBreed){
                // Random chance of failure. This is ok just try again.
                breedFrogsTest();
            }else{
                e.printStackTrace();
                fail(e.getMessage());
            }
        }catch(Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void createPredatorTest() {
        Pond pond = createPond();
        createPredator(pond);
    }

    private Predator createPredator(Pond pond){
        try{
            Predator predator = client.createPredator(pond.getId());

            assertTrue(predator.getId() > 0);
            assertTrue(predator.getPondId() == pond.getId());
            assertNotNull(predator.getLivingState());

            return predator;
        }catch (Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }
        return null;
    }

    @Test
    public void getPredatorTest() {
        Pond pond = createPond();
        Predator predator1 = createPredator(pond);

        try{
            Predator predator2 = client.getPredator(predator1.getId());

            assertTrue(predator2.getId() == predator1.getId());
            assertTrue(predator2.getPondId() == predator1.getPondId());
        }catch (Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void simulateDayTest(){
        Pond pond = createPond();
        Frog frog1 = createFrog(Frog.GenderEnum.Female, pond);
        Frog frog2 = createFrog(Frog.GenderEnum.Female, pond);
        Predator predator = createPredator(pond);

        try{
            Pond pond1 = client.simulateDay(pond.getId());
            assertTrue(pond1.getId() == pond.getId());
            assertTrue(pond1.getDay() == (pond.getDay() + 1));
            assertNotNull(pond1.getFrogWithId(frog1.getId()));
            assertNotNull(pond1.getFrogWithId(frog2.getId()));

        }catch(Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
}