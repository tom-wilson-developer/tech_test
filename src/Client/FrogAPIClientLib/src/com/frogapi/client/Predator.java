package com.frogapi.client;

import net.maritimecloud.internal.core.javax.json.JsonObject;

public class Predator extends Animal {

    private static final String TARGET_KEY = "target";
    private Animal target;


    Predator(JsonObject jsonObject) {
        super(jsonObject);

        if(jsonObject.containsKey(target) && !jsonObject.isNull(TARGET_KEY)){
            target = new Animal(jsonObject.getJsonObject(TARGET_KEY));
        }
    }
}
