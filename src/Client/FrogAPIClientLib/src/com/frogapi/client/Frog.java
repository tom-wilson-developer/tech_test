package com.frogapi.client;

import net.maritimecloud.internal.core.javax.json.JsonObject;

public class Frog extends Animal {

    private static final String MATED_KEY = "mated";
    private static final String VITALITY_KEY = "vitality";
    static final String GENDER_KEY = "gender";

    public enum GenderEnum{
        Unknown, Male, Female
    }
    private GenderEnum gender;
    private boolean mated;
    private int vitality;


    Frog(JsonObject json){
        super(json);

        mated = json.getBoolean(MATED_KEY, false);
        vitality = json.getInt(VITALITY_KEY, 0);
        String genderString = json.getString(GENDER_KEY, null);
        if(genderString != null){
            try{
                gender = GenderEnum.valueOf(genderString);
            }catch(IllegalArgumentException e){
                e.printStackTrace();
            }
        }

    }

    public int getVitality() {
        return vitality;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public boolean hasMated() {
        return mated;
    }
}
