package com.frogapi.client;

public enum Error {

    //Client errors
    ConnectionFailed(101),
    RequestError(102),
    InvalidStatusResponse(103),
    ResponseError(104),
    CommunicationError(105),
    ConnectionClosed(106),
    InvalidUrl(107),
    InvalidResponse(108),
    // Server Errors
    UnknownError(1000),
    PondNotFound(1001),
    FrogNotFound(1002),
    PredatorNotFound(1003),
    MalformedRequestBody(1004),
    InvalidFrogCount(1005),
    FrogsSameGender(1006),
    DifferentPonds(1007),
    AlreadyMated(1008),
    FailedToWrite(1009),
    FailedToBreed(1010);

    private final int value;

    Error(int value){
        this.value = value;
    }

    public static Error errorFromInt(int value){
        for(Error e : Error.values()){
            if(e.value == value){
                return e;
            }
        }

        return UnknownError;
    }

    String getClientErrorMessage(){
        switch(this){
            case ConnectionFailed:
                return "Failed to connect to server";
            case RequestError:
                return "Failed to send request";
            case InvalidStatusResponse:
                return "Invalid status response from server";
            case ResponseError:
                return "Failed to retrieve response";
            case CommunicationError:
                return "Failed to communicate with server";
            case ConnectionClosed:
                return "Connection to server was closed";
            case InvalidUrl:
                return "Failed to build valid ULR";
            case InvalidResponse:
                return "Unexpected response returned from server";
                default:
                    return "Unknown error";
        }
    }
}
