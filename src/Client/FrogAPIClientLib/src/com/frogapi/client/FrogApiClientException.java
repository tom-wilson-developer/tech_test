package com.frogapi.client;

/*
A better idea then using this Exception class may have been to sub class the response class and have them parse
out individual object data. Then returning a Response subclass with the success property. It would save throwing
exceptions but you'd need to have one for each type of response.

Another option could be to have the integration parse the objects json? Builder pattern?
 */

public class FrogApiClientException extends Exception {
    private Error error;
    private String errorDescription;

    public FrogApiClientException(Error error, Exception e){
        super(e);
        this.error = error;
    }

    public FrogApiClientException(Error error, String errorDescription){
        super();
        this.error = error;
        this.errorDescription = errorDescription;
    }

    public Error getError(){
        return error;
    }

    public String getErrorDescription(){
        return errorDescription;
    }
}
