package com.frogapi.client;


import net.maritimecloud.internal.core.javax.json.JsonObject;

public class Animal {

    private static final String ID_KEY = "id";
    private static final String AGE_KEY = "age";
    static final String POND_ID_KEY = "pondId";
    private static final String LIVING_STATE_KEY = "state";
    private static final String DEATH_REASON_KEY = "deathReason";

    public enum LivingStateEnum{
        Unknown,Alive,Dead
    }

    public enum DeathReasonEnum{
        Unknown,OldAge,Eaten,Starvation
    }

    protected long id;
    protected LivingStateEnum livingState;
    protected DeathReasonEnum deathReason;
    protected int age;
    protected long pondId;

    Animal(JsonObject jsonObject){
        id = jsonObject.getInt(ID_KEY, 0);
        age = jsonObject.getInt(AGE_KEY, 0);
        pondId = jsonObject.getInt(POND_ID_KEY, 0);

        String livingStateStr = jsonObject.getString(LIVING_STATE_KEY, LivingStateEnum.Unknown.toString());
        if(livingStateStr != null){
            try{
                livingState = LivingStateEnum.valueOf(livingStateStr);
            }catch(IllegalArgumentException e){
                e.printStackTrace();
            }
        }

        String deathReasonStr = jsonObject.getString(DEATH_REASON_KEY, DeathReasonEnum.Unknown.toString());
        if(deathReasonStr != null){
            try{
                deathReason = DeathReasonEnum.valueOf(deathReasonStr);
            }catch (IllegalArgumentException e){
                e.printStackTrace();
            }
        }

    }

    public long getId(){
        return id;
    }

    public DeathReasonEnum getDeathReason() {
        return deathReason;
    }

    public int getAge() {
        return age;
    }

    public LivingStateEnum getLivingState() {
        return livingState;
    }

    public long getPondId() {
        return pondId;
    }

}
