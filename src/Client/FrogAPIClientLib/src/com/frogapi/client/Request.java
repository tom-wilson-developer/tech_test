package com.frogapi.client;

import net.maritimecloud.internal.core.javax.json.Json;
import net.maritimecloud.internal.core.javax.json.JsonObject;

class Request {

    public JsonObject requestBody;

    public Request(JsonObject jsonObject){
        requestBody = jsonObject;
    }

    public Request(){
        this(Json.createObjectBuilder().build());
    }
}
