package com.frogapi.client;

import net.maritimecloud.internal.core.javax.json.*;
import net.maritimecloud.internal.core.javax.json.stream.JsonParsingException;
import sun.net.www.protocol.http.HttpURLConnection;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Client {

    private static String CONTENT_TYPE = "application/json";
    private URL baseUrl;
    public Client(URL baseUrl){
        this.baseUrl = baseUrl;
    }


    private Response sendGetRequest(URL url){
        return sendRequest(url, "GET");
    }

    private Response sendDeleteRequest(URL url){
        return sendRequest(url, "DELETE");
    }

    // It would be nice to use generics here but java doesn't allow you to initialise with them, meaning you
    // have to pass in an instance or similar. In the end I don't believe i'd gain much from it.
    private Response sendRequest(URL url, String type){
        Response response = null;
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(type);
            connection.setRequestProperty("Accept-Charset", CONTENT_TYPE);

            int responseCode = connection.getResponseCode();
            if(responseCode != 200){
                return new Response(false, Error.InvalidStatusResponse);
            }

            JsonReader reader = Json.createReader(connection.getInputStream());
            response = readResponse(reader);

            reader.close();
            connection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
            response = new Response(false, Error.ConnectionFailed);
        }

        return response;
    }

    private Response sendPostRequest(URL url, Request request){
        Response response = null;
        try{
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Accept-Charset", CONTENT_TYPE);
            connection.setRequestProperty("Content-Type", CONTENT_TYPE);
            connection.setChunkedStreamingMode(0);

            JsonWriter jsonWriter = Json.createWriter(connection.getOutputStream());
            response = writeRequest(jsonWriter, request);

            int responseCode = connection.getResponseCode();
            if(responseCode != 200){
                return new Response(false, Error.ResponseError);
            }

            if(!response.success){
                jsonWriter.close();
                connection.disconnect();
                return response;
            }

            JsonReader jsonReader = Json.createReader(connection.getInputStream());
            response = readResponse(jsonReader);

            jsonWriter.close();
            jsonReader.close();
            connection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
            response = new Response(false, Error.ConnectionFailed);
        }

        return response;
    }

    private Response writeRequest(JsonWriter jsonWriter, Request request){
        try{
            jsonWriter.writeObject(request.requestBody);
        }catch(IllegalStateException e){
            e.printStackTrace();
            return new Response(false, Error.ConnectionClosed);
        }catch(JsonException e){
            e.printStackTrace();
            return new Response(false, Error.CommunicationError);
        }
        return new Response(true, null);
    }

    private Response readResponse(JsonReader jsonReader){
        try{
            return new Response(jsonReader.readObject());
        }catch(JsonParsingException e){
            e.printStackTrace();
            return new Response(false, Error.ResponseError);
        }catch(ClassCastException e){
            e.printStackTrace();
            return new Response(false, Error.ResponseError);
        }catch(JsonException e){
            e.printStackTrace();
            return new Response(false, Error.CommunicationError);
        }
    }

    private Response performPostRequest(String path, Request request) throws FrogApiClientException{
        try {
            URL url = combineUrlPath(baseUrl, path);

            Response response = sendPostRequest(url, request);
            if(!response.success){
                throw new FrogApiClientException(response.error, response.errorMessage);
            }
            return response;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new FrogApiClientException(Error.InvalidUrl, e);
        }
    }

    private Response performGetRequest(String path) throws FrogApiClientException{
        try {
            URL url = combineUrlPath(baseUrl, path);

            Response response = sendGetRequest(url);
            if(!response.success){
                throw new FrogApiClientException(response.error, response.errorMessage);
            }
            return response;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new FrogApiClientException(Error.InvalidUrl, e);
        }
    }

    public Frog createFrog(Frog.GenderEnum gender, long pondId) throws FrogApiClientException {
        String path = "/api/frog";
        JsonObject jsonObject = Json.createObjectBuilder().add(Frog.GENDER_KEY, gender.toString())
                .add(Animal.POND_ID_KEY, pondId)
                .build();


        Response response = performPostRequest(path, new Request(jsonObject));
        if(response.data.getValueType() == JsonValue.ValueType.OBJECT){
            return new Frog((JsonObject)response.data);
        }else{
            throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON object");
        }

    }

    public Frog getFrog(long frogId) throws FrogApiClientException {
        String path = "/api/frog/" + frogId;

        Response response = performGetRequest(path);

        if(response.data.getValueType() == JsonValue.ValueType.OBJECT){
            return new Frog((JsonObject)response.data);
        }else{
            throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON object");
        }
    }


    public Frog breedFrogs(long frog1Id, long frog2Id) throws FrogApiClientException {
        String path = "/api/frog/breed?frog1Id="+frog1Id+"&frog2Id="+frog2Id;

        Response response = performGetRequest(path);

        if(response.data.getValueType() == JsonValue.ValueType.OBJECT){
            return new Frog((JsonObject) response.data);
        }else{
            throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON object");
        }
    }

    public Pond createPond() throws FrogApiClientException{
        String path = "/api/pond";

        Response response = performPostRequest(path, new Request());
        if(response.data.getValueType() == JsonValue.ValueType.OBJECT){
            return new Pond((JsonObject)response.data);
        }else{
            throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON object");
        }

    }

    public Pond deletePond(long id) throws FrogApiClientException {
        String path = "/api/pond/" + id;

        try {
            Response response = sendDeleteRequest(this.combineUrlPath(baseUrl, path));

            if(!response.success){
                throw new FrogApiClientException(response.error, response.errorMessage);
            }
            if(response.data.getValueType() == JsonValue.ValueType.OBJECT){
                Pond pond = new Pond((JsonObject)response.data);
                return pond;
            }else{
                throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON object");
            }

        } catch (MalformedURLException e) {
            throw new FrogApiClientException(Error.InvalidUrl, e);
        }
    }

    public List<Pond> getPonds() throws FrogApiClientException {
        String path = "/api/pond";
        List<Pond> ponds = new ArrayList<>();
        Response response = performGetRequest(path);
        if(response.data.getValueType() == JsonValue.ValueType.ARRAY){
            JsonArray dataArray = (JsonArray)response.data;
            if(dataArray.size() > 0){
                try{
                    for(int i=0; i<dataArray.size();i++){
                        Pond pond = new Pond(dataArray.getJsonObject(i));
                        ponds.add(pond);
                    }
                }catch (ClassCastException e){
                    throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON array");
                }

            }
        }


        return ponds;
    }

    public Pond getPond(long id) throws FrogApiClientException {
        String path = "/api/pond/" + id;
        Response response = performGetRequest(path);

        if(response.data.getValueType() == JsonValue.ValueType.OBJECT){
            return new Pond((JsonObject)response.data);
        }else{
            throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON object");
        }

    }

    public Predator createPredator(long pondId) throws FrogApiClientException {
        String path = "/api/predator";
        JsonObject requestData = Json.createObjectBuilder().add(Animal.POND_ID_KEY, pondId).build();

        Response response = performPostRequest(path, new Request(requestData));

        if(response.data.getValueType() == JsonValue.ValueType.OBJECT){
            return new Predator((JsonObject) response.data);
        }else{
            throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON object");
        }
    }

    public Predator getPredator(long id) throws FrogApiClientException {
        String path = "/api/predator/"+id;

        Response response = performGetRequest(path);

        if(response.data.getValueType() == JsonValue.ValueType.OBJECT){
            return new Predator((JsonObject) response.data);

        }else{
            throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON object");
        }
    }

    public Pond simulateDay(long pondId) throws FrogApiClientException {
        String path = "/api/simulate/"+pondId;

        Response response = performGetRequest(path);

        if(response.data.getValueType() == JsonValue.ValueType.OBJECT){
            return new Pond((JsonObject)response.data);
        }else{
            throw new FrogApiClientException(Error.InvalidResponse, "Response not of type JSON object");
        }
    }


    private URL combineUrlPath(URL baseUrl, String pathCompent) throws MalformedURLException {
        return new URL(baseUrl.getProtocol(), baseUrl.getHost(), baseUrl.getPort(), pathCompent);
    }
}
