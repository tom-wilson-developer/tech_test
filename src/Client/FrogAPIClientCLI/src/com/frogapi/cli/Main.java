package com.frogapi.cli;

import java.net.MalformedURLException;
import java.net.URL;

public class Main {

    public static void main(String[] args) {
	// write your code here
        if(args.length > 0){
            try{
                URL url = new URL(args[0]);
                CliApplication cli = new CliApplication(url);

                cli.runApplication();
            } catch (MalformedURLException e) {
                System.out.println("Error: Invalid URL supplied: ["+args[0]+"]");
            }
        }else{
            System.out.println("No url supplied");
        }
    }
}
