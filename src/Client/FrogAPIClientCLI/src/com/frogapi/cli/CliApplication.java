package com.frogapi.cli;

import com.frogapi.client.*;

import java.net.URL;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CliApplication {

    private Client client;
    private Boolean running = true;
    private Scanner input = new Scanner(System.in);
    public CliApplication(URL url){
        client = new Client(url);
    }

    public void runApplication(){

        while(running){
            printMenu();

            char inputCha;
            try{
                inputCha = input.next().charAt(0);
            }catch(NoSuchElementException e){
                //thrown when application is force closed.
                quit();
                return;
            }


            try{
                switch (inputCha){
                    case 'p': case 'P':
                        Pond pond = client.createPond();
                        println("Pond created successfully");
                        printPond(pond);
                        break;
                    case 'f' : case 'F':
                        println("Creating frog");
                        createFrog();
                        break;
                    case 'r' : case 'R':
                        println("Creating predator");
                        createPredator();
                        break;
                    case 's' : case 'S':
                        println("Beginning simulation");
                        simulateDay();
                        break;
                    case 'q':case 'Q':
                        quit();
                        break;
                    default:
                        println("Unrecognised command");
                }
            } catch (FrogApiClientException e) {
                println("Process failed with error: "+e.getError().toString());
                println(e.getErrorDescription());
            }


        }

    }

    private void printMenu(){
        println("P - Create Pond");
        println("F - Create Frog");
        println("R - Create Predator");
        println("S - Simulate Day");
        println("Q - Quit");
        println("Enter a letter:");
    }

    private void print(String s){
        System.out.print(s);
    }
    private void println(String s){
        System.out.println(s);
    }

    private void printPond(Pond p){
        println("Pond ID: "+p.getId());
        println("Pond Day: "+p.getDay());
        println("Frog count: " +p.getFrogs().size());
        println("Predator count: "+p.getPredators().size());
        println("");
        println("Frogs: [");
        for(Frog f : p.getFrogs()){
            println("");
            print("[");
            printFrog(f);
            print("]");

        }
        println("]");

        println("Predators: [");
        for(Predator pr : p.getPredators()){
            print("[");
            printPredator(pr);
            print("]");

        }
        println("]");
        println("");
    }

    private void printFrog(Frog frog){
        println("Frog ID:" + frog.getId());
        println("Frog Age: " + frog.getAge());
        println("Frog Gender:" + frog.getGender().toString());
        println("Lives In Pond: " + frog.getPondId());
        println("Frog State:" + frog.getLivingState().toString());
        if(frog.getLivingState() == Animal.LivingStateEnum.Dead){
            println("Cause Of Death: " + frog.getDeathReason().toString());
        }
    }

    private void printPredator(Predator p){
        println("Predator ID: "+p.getId());
        println("Predator Age: "+p.getAge());
        println("Hunts In Pond: "+p.getPondId());
        println("Predator State: "+p.getLivingState().toString());
        if(p.getLivingState() == Animal.LivingStateEnum.Dead){
            println("Cause Of Death: " + p.getDeathReason().toString());
        }
    }

    private void createFrog(){
        try{
            Frog.GenderEnum gender = getGenderInput();
            long pondId = getPondIdSelection();
            Frog frog = client.createFrog(gender, pondId);
            println("Successfully created frog");
            printFrog(frog);

        } catch (FrogApiClientException e) {
            println("Failed to create frog");
            println("Error: " + e.getError().toString());
            println(e.getErrorDescription());
        }

    }

    private void createPredator(){
        try{
            long pondId = getPondIdSelection();
            Predator predator = client.createPredator(pondId);
            println("Predator created successfully");
            printPredator(predator);
        } catch (FrogApiClientException e) {
            println("Failed to create frog");
            println("Error: " + e.getError().toString());
            println(e.getErrorDescription());
        }
    }

    private void simulateDay(){
        try{
            long pondId = getPondIdSelection();
            Pond pond = client.simulateDay(pondId);

            println("Simulation ran successfully");
            println("Result");
            printPond(pond);

        } catch (FrogApiClientException e) {
            println("Failed to run simulation");
            println("Error: " + e.getError().toString());
            println(e.getErrorDescription());
        }
    }

    private Frog.GenderEnum getGenderInput(){
        println("Select Gender: [M = male, F = female]");
        char genderChar = input.next().charAt(0);
        switch (genderChar){
            case 'm' : case 'M':
                return Frog.GenderEnum.Male;
            case 'f' : case 'F':
                return Frog.GenderEnum.Female;
                default:{
                    println("Invalid gender");
                    return getGenderInput();
                }
        }
    }

    private long getPondIdSelection() throws FrogApiClientException {
        try{
            List<Pond> ponds = client.getPonds();
            println("Select pond by id");
            for(Pond p : ponds){
                print(p.getId() +", ");
            }
            println("");
            long pondId = input.nextLong();
            return pondId;
        }catch (InputMismatchException e){
            println("Invalid input try again");
            return getPondIdSelection();
        }

    }

    private void quit(){
        running = false;
        input.close();
        println("Closing application");
    }
}
