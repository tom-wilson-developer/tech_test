
<!---
Add your comments / notes and thoughts to this doc

Any special instructions to running your code?
-->
# The Plan
The requirement is to build a system to manage a pond of frogs. There will be two parts i would like to do for this.The first will be a Web API built in .NET core. This will allow clients to interact with the pond and will be in charge of the main business logic of the system. The second part will be a client which connects to the Web API and handle the information exchange between the Web API and the integrating application.

# Requirements
Following are the requirements I have set myself for this test.

* web api
* client
* database
* features
	* life span of frog
	* mating
	* environments
* optional features if time allows:
	* feeding
	* session tracking
	* client integration

# Technical overview
This section will outline the technical choices and why they were made.

## Web API
I chose to create a Web API as it allowed for greater flexibility in what can interact with the management system. As the API will communicate using a common syntax such as JSON, I could choose any technology I wanted to build the client.

### .NET Core
I chose to use .NET Core for the API as the role I am applying for is predominantly a C# role. My personal computer is also a Mac which limited my options if I wanted to make a Web API in C#.

## Client
I wanted to build a client as an easy way to demo the Web API. I also found in the passed that a good way to test the design of an API is integrate with it. Often an API can look good on paper but could miss quality of life elements that make it great. It also allows me a chance to play to my strengths, as much of my background has been in client side SDKs.

My current thoughts on client technology is to use java. The reasons being are:
* It is a multi platform language
* It's very well supported
* It's a language I'm very familiar with.

#To Run

### Client cli
navigate to the jar in src/Build/CLI and run the following command

java -jar FrogAPIClientCLI.jar http://localhost:5000

### Web API
I've included a self contained build of the web api in src/build/API/win7-x64.zip I haven't been able to test this but I believe it can be run by unzipping the contents and from the public directory running

dotnet run FrogPondAPI.dll

However there is also an .exe which could be used.

When running this will log the host which can be connected to, by default this is:

http://localhost:5000

# Closing comments
I think I underestimated how long the API side would take me, when I started I was still trying to think of things to add and I was stuck by some things such as foreign key constraints. It was the first time using .netcore and building a webAPI in c#.
Largely my experience is client side so was unsure sometime weather behaviour should be part of the API or the client.

I ended up replacing some ideas with others, predators was an idea I quit liked so chose to add them over environments.

I spent about 3 days on the project in the end.

## Things to improve
* More tests. Tests on the client currently are quite bare bones and are more for demonstration.
* Logging. Logging throughout the application would help with debugging in a production environment.
* Code clean up. 
	* There is some repeated code in the client which could be refactored and made neater.
	* Such as casting to JsonObject. It may have been better to have methods in the Response object to perform this. eg Response.getObjectData, Response.getArrayData()
* Build out the functionality of the API more.
* It would have been nice to do more with the simulation and while a method to breed is available I didn't get time to add it into the simulation.